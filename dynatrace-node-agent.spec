%define _topdir     /home/build/rpmbuild
%define _temppath   %{_topdir}/tmp
%define osedir      /usr/libexec/openshift/cartridges/dynatrace-node-agent
%define name        dynatrace-node-agent
%define release     0
%define version     0.1
%define buildroot %{_topdir}/%{name}-%{version}-root

BuildRoot:      %{buildroot}
Summary:        dynatrace node agent
License:        TBD
Name:           %{name}
Version:        %{version}
Release:        %{release}
Source:         %{name}-%{version}.tar.gz
Group:          Development/Tools

%description
dynaTrace webserver agent OpenShift cartridge for Node.js applications

%install
mkdir -p %{buildroot}%{osedir}
cp -R * %{buildroot}%{osedir}

%files
%dir %{osedir}
%attr(0755,-,-) %{osedir}/bin/
%{osedir}/env
%{osedir}/logs
%{osedir}/metadata
%{osedir}/run
%{osedir}/versions
%doc %{osedir}/README.md
