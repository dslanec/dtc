module.exports = exports = nodejsagent = function(options) {

var verID = '4.0.0005';

// external dependency; use "npm install node-uuid"
var uuid = require('node-uuid');

var createNamespace = require('continuation-local-storage').createNamespace;
var session = createNamespace('ruxitSession');

// standard nodeJS library
var module      = require('module');
var buff        = require('buffer');
var util        = require('util');
var url         = require('url');
var path        = require('path');
var http        = require('http');
var querystring = require('querystring');


// some enums
var REQUEST_TYPE = { REGULAR:   0, AGENT:    1, BEACON: 2,             UNKNOWN:  5 };
var INJECTION    = { NEED_MORE: 1, INJECTED: 2, FLUSH:  3, UNKNOWN: 4, DISABLED: 5 };

var ENCODING     = { '': 0, 'gzip': 1, 'deflate': 2};

var EMPTY_ID = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx';


// config
var JSAconfig = function(cfg_filename) {
    var self = {};

    var _cfg = require(cfg_filename || './nodejsagent.json');

    self.logger = {
        DEBUG   : 1,
        REQ     : 2,
        INFO    : 3,
        WARN    : 4,
        ERROR   : 5,
        NONE    : 10,

        _lvl    : 2,
        get level()      { return this._lvl; },
        set level(l)     { if (this[l]) { this._lvl = this[l]; } },

        _lib    : 'nodejs',   // 'nodejs' || 'native'
        get library()    { return this._lib; },
        set library(l)   { if (l === 'nodejs' || l === 'native') { this._lib = l; } },

        get toNative()   { return (self.cfgAgent !== null && this.library === 'native'); },
        get toConsole()  { return (this.library === 'nodejs' || !this.toNative); },

        log     : function(msg) { console.log(msg); },
        debug   : function(msg) { if (this.level <= this.DEBUG) { if (this.toNative) self.cfgAgent.log(0, msg); if (this.toConsole) console.info(msg);  }},
        req     : function(msg) { if (this.level <= this.REQ)   { if (this.toNative) self.cfgAgent.log(3, msg); if (this.toConsole) console.info(msg);  }},
        info    : function(msg) { if (this.level <= this.INFO)  { if (this.toNative) self.cfgAgent.log(4, msg); if (this.toConsole) console.info(msg);  }},
        warn    : function(msg) { if (this.level <= this.WARN)  { if (this.toNative) self.cfgAgent.log(5, msg); if (this.toConsole) console.warn(msg);  }},
        error   : function(msg) { if (this.level <= this.ERROR) { if (this.toNative) self.cfgAgent.log(7, msg); if (this.toConsole) console.error(msg); }},
        res     : function(err, msg) { if (err < 400) this.req(msg); else this.warn(msg); }
    };

    self.logger.level     = _cfg.logLevel;
    self.logger.library   = _cfg.logLibrary;

    self.injectionMapping = [];
    Object.keys(_cfg.injectionMapping).forEach(function(key) {
        self.injectionMapping.push([new RegExp(key, 'im'), _cfg.injectionMapping[key]]);
    });

    self.nativelib                    = _cfg.nativelib                    || 'ruxitnodejsagent';
    self.native                       = null;
    self.cfgAgent                     = null;

    self.specHeader                   = _cfg.specHeader                   || 'x-dynatrace';

    self.JS_PREFIX                    = _cfg.originalMethodPrefix         || '_JS_';
    self.DEF_APPLICATIONNAME          = _cfg.defaultAppName               || 'nodejsApplication';

    self.specialInstrumentedFunctions = _cfg.specialInstrumentedFunctions || {};
    self.instrumentationInclude       = _cfg.instrumentationInclude       || [];
    self.instrumentationExclude       = _cfg.instrumentationExclude       || [];
    self.instrumentationDelayed       = _cfg.instrumentationDelayed       || [];

    self._agentActive = false;
    Object.defineProperty(self, 'agentActive', {
        get: function()  { return self._agentActive; },
        set: function(a) { self._agentActive = (a === 1 || a === '1' || a === 'true' || a === true); }
    });
    self.agentActive = _cfg.agentActive;

    self._nativeActive = false;
    Object.defineProperty(self, 'nativeActive', {
        get: function()  { return self.agentActive && self._nativeActive; },
        set: function(a) { self._nativeActive = (a === 1 || a === '1' || a === 'true' || a === true); }
    });
    self.nativeActive = _cfg.nativeActive;

    self._appName = '';
    Object.defineProperty(self, 'applicationName', {
        get: function()  { return self._appName; },
        set: function(n) { self._appName = n; }
    });

    // if app was started by "npm start"
    var appName = global.process.env.npm_package_name;
    if (appName) {
        self.applicationName = appName;
    }
    else {
        // try to find package.json file
        try {
            var appDir = path.dirname(global.process.mainModule.filename);
            appName = require(path.join(appDir, 'package.json')).name;
            if (appName) {
                self.applicationName = appName;
            }
        }
        catch (err) {}
    }
    if (!self.applicationName) {
        // default
        self.applicationName = self.DEF_APPLICATIONNAME;
    }

    // main instrumentation logic
    self.toInstrument = function(entityName) {

        //----------------------------------------------------------------------
        var arr_getElementsCovering = function(lst, arg) {
            return lst.filter(function(elm, idx, arr) {
                return elm.split('@').every(function(xelm, xidx, xarr) {
                    return (!this[xidx] || xelm === this[xidx]);
                }, this.split('@'));
            }, arg);
        };

        var arr_hasElementCovering = function(lst, arg) {
            return arr_getElementsCovering(lst, arg).length > 0;
        };

        var arr_hasLongerElement = function(lst, arg) {
            return arr_getElementsCovering(lst, arg).filter(function(elm, idx, arr) {
                return elm.length > this.length;
            }, arg).length > 0;
        };

        var arr_hasElement = function(lst, arg) {
            return lst.indexOf(arg) > -1;
        };

        var arr_getNotLongerPath = function(lst, arg) {
            return arr_getElementsCovering(lst, arg).filter(function(elm, idx, arr) { return elm.split('@').length <= this; }, arg.split('@').length);
        };

        var arr_getMatchLevel = function(lst, arg) {
            return arg.split('@').length - Math.max.apply(null, arr_getNotLongerPath(lst, arg).map(function(elm, idx, arr) { return elm.split('@').length; }, arg.split('@').length));
        };
        //----------------------------------------------------------------------

        if (arr_hasElementCovering(Object.keys(self.specialInstrumentedFunctions), entityName) || arr_hasElement(self.instrumentationInclude, entityName)) return true;
        if (arr_hasElement(self.instrumentationExclude, entityName)) return (!str_endsWith(entityName, '()') && arr_hasLongerElement(self.instrumentationInclude, entityName));
        return ((arr_getMatchLevel(self.instrumentationInclude, entityName) < arr_getMatchLevel(self.instrumentationExclude, entityName)) ||
                 (arr_hasElement(self.instrumentationInclude, '*') && arr_getMatchLevel(self.instrumentationExclude, entityName) === Infinity) ||
                 arr_hasLongerElement(self.instrumentationInclude, entityName));
    };

    self.toInstrumentDelayed = function(functionName) {
        return self.instrumentationDelayed.indexOf(functionName) > -1;
    };

    return self;
};


//+++ some universal functions
var str_endsWith   = function(str, sub) { return (str.substr(str.length - sub.length, sub.length) === sub); };
var str_startsWith = function(str, sub) { return (str.substr(0, sub.length) === sub); };

var stringify = function(obj) {
    var cache = [];
    var ret = JSON.stringify(obj, function(key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) return;
            cache.push(value);
        }
        return value;
    });
    cache = null;
    return ret;
};

var makeArgumentsCopy = function() {
    var new_args = [];
    for (var k = 0; k < arguments.length; k++) { new_args.push(arguments[k]); }
    return new_args;
};

var timer = function(tm, r) {
    if (!tm) {
        return process.hrtime();
    }
    var td = process.hrtime(tm);
    var ds, dn, dt = 0;
    switch (r) {
        case 'sec'   : ds = 1  ; dn = 1e9; break;
        case 'mili'  : ds = 1e3; dn = 1e6; break;
        case 'micro' : ds = 1e6; dn = 1e3; break;
        case 'nano'  : ds = 1e9; dn = 1  ; break;
        default      : ds = 1e3; dn = 1e6; break;
    }
    try {
        dt = parseInt(td[0] * ds + td[1] / dn, 10);
    }
    catch (err) { }
    return dt;
};
//--- some universal functions


//+++ for req-res handling
var getClientAddress = function (req) {
    return (req.headers['x-forwarded-for'] || '').split(',')[0] ||
        req.connection.remoteAddress;
};

var parseCookie = function(cookie) {
    var out = {};
    if (cookie) {
        cookie.split('; ').forEach(function(el) {
            var e = el.split('=');
            out[e.shift()] = e.join('=');
        });
    }
    return out;
};

// for incoming requests
js_addHeaderLine = function(obj, args, log_functionName) {
    // args = [field, value]

    //+++ preexecutive part
    if (cfg.agentActive) {
        if (!this.allHeaders) { this.allHeaders = []; }
        this.allHeaders.push(args[0] + ': ' + args[1]);
    }
    //--- preexecutive part

    //+++ executive part
    var ret = obj.apply(this, args);
    //--- executive part

    //+++ postexecutive part
    //if (cfg.agentActive) { }
    //--- postexecutive part

    return ret;
};
//--- for req-res handling


//+++ injection
var injection = function(xres, xchunk, xenc) {
    var id       = (xres._JS_metadata ? xres._JS_metadata.id : EMPTY_ID);
    var ret      = { InjectedFrag: xchunk, Injected: INJECTION.NEED_MORE, TagLen: 0 };

    // first: dumb html injection
    //&    "injectionMapping" : {
    //&        "<head[^>]*>": "<script type=\"text/javascript\" src=\"/dtagent05_jn_1399622313.js\" data-dtconfig=\"rid=RID_1881764452|rpid=298448057\"></script>",
    //&        "<body[^>]*>": "DUMB HTML INJECTION"
    //&    }

    /*
    if (data) {
        if (cfg.injectionMapping) {
            cfg.injectionMapping.forEach(function(key) {
                if (key[0].test(data)) {
                    log.info('[' + id + ']   >>> dumb injection');
                    data = data.replace(key[0], '$&' + key[1]);
                    //injLen += Buffer.byteLength(key[1]);
                    injLen += key[1].length;
                }
            });
        }
    }
    */

    if (cfg.nativeActive) {
        var b = null;
        if (xchunk === null || xchunk === '') { b = new Buffer(0); }             // xchunk is empty (null or '')
        else if (typeof xchunk === 'string')  { b = new Buffer(xchunk, xenc); }  // xchunk is not empty string
        else                                  { b = xchunk; }                    // xchunk is buffer
        var url_parts = url.parse(xres._JS_metadata.url);
        log.debug('[' + id + ']   >>> native.injectJsAgent()');
        ret = xres._JS_metadata.reqProc.injectJsAgent(b, ENCODING[xres._JS_metadata.content.encoding], url_parts.pathname, xres._JS_metadata.clientIP);
        if (ret.Injected === INJECTION.INJECTED) {
            log.info('[' + id + ']   >>> real injection (' + ret.TagLen.toString(10) + ' B) : "' + xres._JS_metadata.method + ' ' + xres._JS_metadata.url + '"');
        }
    }

    return ret;
};
//--- injection


// change res.end(data) to sequence res.write(data); res.end()
js_end = function(obj, args, log_functionName) {
    // args = [data, encoding]

    var ret = null;
    var metadata = (this.socket ? this.socket._httpMessage._JS_metadata : null);
    var id = (metadata ? metadata.id : EMPTY_ID);

    if (cfg.agentActive) {
        if (args[0] && metadata) {
            log.debug('[' + id + '] >>> Calling res.write(data)...');
            this.write.apply(this, args);
            var new_args = makeArgumentsCopy.apply(this, args);
            new_args[0] = '';
            log.debug('[' + id + '] >>> ...and then res.end()');
            return obj.apply(this, new_args);
        }
    }

    log.debug('[' + id + '] >>> Calling regular res.end([data])');
    return obj.apply(this, args);
};


js_send = function(obj, args, log_functionName) {
    // args = [data, encoding, callback]

    var ret = null;
    var metadata = (this.socket ? this.socket._httpMessage._JS_metadata : null);
    var id = (metadata ? metadata.id : EMPTY_ID);

    var isString  = (typeof args[0] === 'string');
    var isChunked = this.chunkedEncoding;   // Transfer-Encoding
    var isNull    = (args[0] === null);
    var enc  = (typeof args[1] === 'string' ? args[1] : undefined);
    var data = '';
    var lenx = 0;
    var isBuffered   = false;
    var sendModified = false;

    //+++ preexecutive part
    if (isNull || !metadata) {
        // data = ''; lenx = 0;
    }
    else if (isChunked) {
        lenx = parseInt(args[0].toString(enc), 16);
        var i = 1;
        for (; i < args[0].length - 2; i++) {
            if (args[0].toString(enc).slice(i, i + 2) === '\r\n') break;
        }
        data = args[0].slice(i + 2, -2);
    }
    else {
        data = args[0],
        lenx = (isString ? Buffer.byteLength(data, enc) : data.length);
    }
    //--- preexecutive part

    // now we have:
    // data - data in initial format (string or Buffer) but 'unchunked' (if it was before)
    // lenx - length of data in bytes (excluding 'chunk envelope')

    //+++ executive part

    // check if the injection can be made
    if (cfg.agentActive && metadata && (!metadata.content.type || str_startsWith(metadata.content.type, 'text/html')) && metadata.injState === INJECTION.NEED_MORE && metadata.reqType === REQUEST_TYPE.REGULAR) {

        var r = injection(this, data, enc);
        metadata.injState = r.Injected;

        // we're waiting for more data, no call to _send()
        if (r.Injected === INJECTION.NEED_MORE && r.InjectedFrag.length === 0 && lenx !== 0) {
            isBuffered = true;
            metadata.buffered += lenx;
        }
        // no injection has been made but that was last time we can inject, just flush buffer
        // call to _send() is obligatory
        else if (r.Injected !== INJECTION.INJECTED) {
            metadata.sent += lenx;
            sendModified = true;
            if (metadata && metadata.hdrDelay) { metadata.hdrSend = true; this.writeHead(); }
            ret = obj.apply(this, args);
        }
        // most interesting case - the js agent has been injected
        // call to _send() is obligatory
        else {
            // convert return data (probably buffer) to proper type (buffer or string / chunked or not)
            // first - data
            var new_data = null;
            var out_data = null;

            if (isString) {                                new_data = r.InjectedFrag.toString(enc); }
            else if (typeof r.InjectedFrag === 'string') { new_data = new Buffer(r.InjectedFrag, enc); }
            else {                                         new_data = r.InjectedFrag; }

            var new_lenx = (isString ? Buffer.byteLength(new_data) : new_data.length);
            if (metadata.content.length) { metadata.content.length = metadata.content.length - metadata.sent - metadata.buffered - lenx + new_lenx; }
            lenx = new_lenx;

            // second - chunking
            if (isChunked) {
                var len_hex;
                if (isString) {
                    // string
                    len_hex  = new_lenx.toString(16);
                    out_data = [len_hex, new_data, ''].join('\r\n');
                }
                else {
                    // buffer
                    var sep  = new Buffer('\r\n');
                    len_hex  = new Buffer(new_lenx.toString(16));
                    out_data = Buffer.concat([len_hex, sep, new_data, sep]);
                }
            }
            else {
                out_data = new_data;
            }

            var new_args = makeArgumentsCopy.apply(this, args);
            new_args[0] = out_data;

            if (metadata && metadata.hdrDelay) { metadata.hdrSend = true; this.writeHead(); }
            ret = obj.apply(this, new_args);
        }
    }
    // don't do anything, just perform regular _send()
    else {
        // but check if there are delayed headers
        if (metadata && metadata.hdrDelay) { metadata.hdrSend = true; this.writeHead(); }
        ret = obj.apply(this, args);
    }
    //--- executive part

    //+++ postexecutive part
    if (cfg.agentActive && metadata) {

        // chunk was buffered
        if (isBuffered) {
            log.debug('[' + id + '] _send() [buffered] -> ' + lenx.toString(10));
        }
        // chunk was sent
        else if (lenx !== 0) {
            if (!sendModified) { metadata.sent += lenx; }
            log.debug('[' + id + '] _send() [' + lenx.toString(10) + ']');
        }
        // last chunk - end of transmission
        else {
            log.debug('[' + id + '] _send() -> ' + metadata.sent.toString(10));
            if (metadata.reqType === REQUEST_TYPE.REGULAR) {
                if (cfg.nativeActive) {
                    if (!metadata.wasClosed) {
                        log.debug('[' + id + ']   >>> native.handleResponse()');
                        metadata.reqProc.handleResponse(
                            this.statusCode,
                            this._header,
                            this._header.length
                        );
                        metadata.wasClosed = true;
                    }
                    else {
                        log.warn('[' + id + '] handleResponse was already invoked!');
                        log.warn(util.inspect(this.socket));
                    }
                }
            }
            log.res(this.statusCode, '[' + id + '] ' + metadata.method + ' ' + metadata.url + ' [' + this.statusCode + '] : ' + timer(metadata.started, 'mili').toString() + ' ms - ' + metadata.sent.toString(10) + ' B');
        }
    }
    //--- postexecutive part

    return ret;
};


js_writeHead = function(obj, args, log_functionName) {
    // args: [statusCode, reasonPhrase, headers, callback]

    var ret = null;
    var metadata = (this.socket ? this.socket._httpMessage._JS_metadata : null);
    var id = (metadata ? metadata.id : EMPTY_ID);
    var int_call = true;

    log.debug('[' + id + '] ..... res.writeHead() <begin>');

    //+++ preexecutive part
    var alreadySent = (!metadata && this._headerSent) || (metadata && metadata.hdrSent);
    if (alreadySent) { int_call = false; }

    if (cfg.agentActive && !alreadySent) {

        if (metadata && metadata.reqType === REQUEST_TYPE.REGULAR) {

            if (metadata.hdrDelay) {
                if (!metadata.hdrSend) {
                    log.debug('[' + id + ']  ... Still waiting with res.writeHead()');
                    int_call = false;
                }
                else {
                    // perform delayed writeHead()
                    log.debug('[' + id + ']  ... Performing delayed res.writeHead()');
                    this._headerSent = false;
                    this.setHeader('Content-Length', metadata.content.length);
                    args = metadata.hdrArgs;
                    metadata.hdrDelay = false;
                }
            }

            // regular writeHead()
            else {
                var hdrArgsX = 1;
                if (typeof args[hdrArgsX] === 'string') { hdrArgsX = 2; }
                var headers = args[hdrArgsX] || {};

                var ct = this.getHeader('Content-Type') || headers['Content-Type'] || 'text/html';

                if (metadata.injState === INJECTION.NEED_MORE && str_startsWith(ct, 'text/html')) {
                    if (this.getHeader('Content-Length') || headers['Content-Length'] || metadata.content.length > 0) {
                        log.debug('[' + id + ']  ... Delay res.writeHead()');
                        if (headers['Content-Length']) {
                            this.setHeader('Content-Length', headers['Content-Length']);
                            delete headers['Content-Length'];
                        }
                        if (this.getHeader('Content-Length')) {
                            var tmp_cl = metadata.content.length;
                            this.removeHeader('Content-Length');
                            metadata.content.length = tmp_cl;
                        }
                        metadata.hdrArgs  = args;
                        metadata.hdrDelay = true;
                        this._headerSent  = true;
                        int_call = false;
                    }
                }
            }
        }
    }
    //--- preexecutive part

    //+++ executive part
    if (int_call) {
        ret = obj.apply(this, args);
        if (metadata) metadata.hdrSent = true;
    }
    //--- executive part

    //+++ postexecutive part
    // if (cfg.agentActive) { }
    //--- postexecutive part

    log.debug('[' + id + '] ..... res.writeHead() <end>');

    return ret;
};


js_storeHeader = function(obj, args, log_functionName) {
    // args = [firstLine, headers]

    var ret = null;
    var metadata = (this.socket ? this.socket._httpMessage._JS_metadata : null);
    var id = (metadata ? metadata.id : EMPTY_ID);
    var headers = args[1] || {};

    log.debug('[' + id + '] ..... res._storeHeader() <begin>');

    //+++ preexecutive part
    if (cfg.agentActive) {
        if (metadata && metadata.reqType === REQUEST_TYPE.REGULAR) {
            Object.keys(headers).forEach(function(el) {
                log.debug('[' + id + ']   ... ' + el + ' -> ' + headers[el]);
            });
            metadata.content.type       = metadata.content.type       || headers['Content-Type']      || '';
            metadata.content.encoding   = metadata.content.encoding   || headers['Content-Encoding']  || '';
            metadata.content.length     = metadata.content.length     || headers['Content-Length']    || 0;
            try { metadata.content.length = parseInt(metadata.content.length, 10); } catch(err) {}
        }
        else {
            //log.warn('No metadata visible in _storeHeader()');
            // no message; most probably this is outgoing request
        }
    }
    //--- preexecutive part

    //+++ executive part
    ret = obj.apply(this, args);
    //--- executive part

    //+++ postexecutive part
    // if (cfg.agentActive) { }
    //--- postexecutive part

    log.debug('[' + id + '] ..... res._storeHeader() <end>');

    return ret;
};


js_setHeader = function(obj, args, log_functionName) {
    // args = [name, value]

    var ret = null;
    var metadata = (this.socket ? this.socket._httpMessage._JS_metadata : null);
    var id = (metadata ? metadata.id : EMPTY_ID);

    log.debug('[' + id + '] ..... res.setHeader() <begin>');

    //+++ preexecutive part
    if (cfg.agentActive) {
        if (metadata && metadata.reqType === REQUEST_TYPE.REGULAR) {
            switch (args[0]) {
                case 'Content-Type'     : log.debug('[' + id + ']  -> Content-Type');      metadata.content.type     = args[1]; break;
                case 'Content-Encoding' : log.debug('[' + id + ']  -> Content-Encoding');  metadata.content.encoding = args[1]; break;
                case 'Content-Length'   : log.debug('[' + id + ']  -> Content-Length');    metadata.content.length   = args[1];
                                          try { metadata.content.length = parseInt(metadata.content.length, 10); } catch(err) {} break;
            }
        }
    }
    //--- preexecutive part

    //+++ executive part
    log.debug('[' + id + ']   * Setting header: [' + args[0] + '] -> [' + args[1] + ']');
    ret = obj.apply(this, args);
    //--- executive part

    //+++ postexecutive part
    // if (cfg.agentActive) { }
    //--- postexecutive part

    log.debug('[' + id + '] ..... res.setHeader() <end>');

    return ret;
};


js_removeHeader = function(obj, args, log_functionName) {
    // args = [name, value]

    var ret = null;
    var metadata = (this.socket ? this.socket._httpMessage._JS_metadata : null);
    var id = (metadata ? metadata.id : EMPTY_ID);

    log.debug('[' + id + '] ..... res.removeHeader() <begin>');

    //+++ preexecutive part
    if (cfg.agentActive) {
        if (metadata && metadata.reqType === REQUEST_TYPE.REGULAR) {
            switch (args[0]) {
                case 'Content-Length'   : log.debug('[' + id + ']  -> Content-Length');    metadata.content.length = 0; break;
            }
        }
    }
    //--- preexecutive part

    //+++ executive part
    ret = obj.apply(this, args);
    //--- executive part

    //+++ postexecutive part
    // if (cfg.agentActive) { }
    //--- postexecutive part

    log.debug('[' + id + '] ..... res.removeHeader() <end>');

    return ret;
};


// universal wrapper - shows input and output of wrapper function
js_debug = function(obj, args, log_functionName) {
    log.info('>>> ' + log_functionName + ' <begin>');
    log.info('(IN) : ' + util.inspect(args));
    var ret = obj.apply(this, args);
    log.info('(OUT): ' + util.inspect(ret));
    log.info('>>> ' + log_functionName + ' <end>');
    return ret;
};


js_httpRequest = function(obj, args, log_functionName) {
    // arguments: [options, callback]
    var options  = args[0];
    var callback = args[1];

    var res = session.get('res');

    if (!res) {
        // no session context - probably internal (not related to any request) outgoing request
        //log.warn('No session data!');
        return obj.apply(this, args);
    }

    var ret = null;
    var metadata = res._JS_metadata || null;
    var id = (metadata ? metadata.id : EMPTY_ID);

    var waitTimeStart = 0;
    log.debug('[' + id + '] ..... http.request() <begin>');
    var started = timer();

    var outUrl = (typeof options === 'string') ? url.parse(options) : util._extend({}, options);
 
    outUrl.protocol = outUrl.protocol || (outUrl.agent && outUrl.agent.options ? outUrl.agent.options.protocol : '') || 'http:';
    outUrl.scheme   = outUrl.protocol.slice(0, -1);
    outUrl.port     = parseInt(outUrl.port || (outUrl.protocol === 'https:' ? 443 : 80), 10);
    outUrl.method   = outUrl.method || 'GET';
    outUrl.appName  = cfg.applicationName;
 
    var tmpUrl = (typeof options === 'string') ? util._extend({}, outUrl) : url.parse(url.format(outUrl));
 
    outUrl.hostname = outUrl.hostname || tmpUrl.hostname || outUrl.host || tmpUrl.host;
    outUrl.host     = outUrl.host     || tmpUrl.host     || outUrl.hostname + (((outUrl.protocol === 'http:' && outUrl.port === 80) || (outUrl.protocol === 'https:' && outUrl.port === 443)) ? '' : ':' + outUrl.port);
    outUrl.pathname = outUrl.pathname || tmpUrl.pathname || '/';
    outUrl.query    = outUrl.query    || tmpUrl.query    || '';
    outUrl.path     = (outUrl.path    || tmpUrl.path     || '/') + (outUrl.query ? '?' + outUrl.query : '');
 
    outUrl.href     = outUrl.href     || tmpUrl.href     || outUrl.protocol + '//' + outUrl.host + outUrl.path;
    outUrl.uri      = outUrl.pathname;
    outUrl.log      = outUrl.method + ' ' + outUrl.href;
    outUrl.dtCookie = res._JS_metadata.dtCookie;



    if (cfg.nativeActive) {

        log.debug('[' + id + '] >>> native.handleOutgoingRequestStart()');

        var outData   = res._JS_metadata.reqProc.handleOutgoingRequestStart(outUrl);
        var newHeader = outData.header;
        var methodId  = outData.methodId;

        if (!outUrl.headers) { outUrl.headers = {}; }
        // add x-dynatrace header
        if (newHeader) { outUrl.headers[cfg.specHeader] = newHeader; }
        // add dtCookie
        if (res._JS_metadata.dtCookie) {
            var setcookieval = outUrl.headers.Cookie ? outUrl.headers.Cookie.split(',') : [];
            setcookieval.push('dtCookie=' + res._JS_metadata.dtCookie + ';path=/;');
            outUrl.headers.Cookie = setcookieval;
        }
    }

    log.req('[' + id + '] ' + outUrl.log + ' [---] : - ms');

    var httpRequestCB = function() {
        // arguments: [resx]

        var resx = arguments[0];
        var id = res._JS_metadata.id;

        var waitTime = timer(waitTimeStart, 'micro');

        // remove x-dynatrace header
        try { delete outUrl.headers[cfg.specHeader]; }
        catch (err) {}

        var ret = null;
        if (callback) { ret = callback.apply(this, arguments); }

        if (cfg.nativeActive) {
            log.debug('[' + id + '] >>> native.handleOutgoingRequestEnd()');
            res._JS_metadata.reqProc.handleOutgoingRequestEnd(methodId, resx.statusCode);
        }
        log.res(resx.statusCode, '[' + id + '] ' + outUrl.log + ' [' + resx.statusCode + '] : ' + timer(started, 'mili').toString() + ' ms');

        return ret;
    };

    var new_args = makeArgumentsCopy.apply(this, args);
    new_args[0] = outUrl;
    new_args[1] = httpRequestCB;

    ret = obj.apply(this, new_args);

    ret.on('error', function(e) {
        var statusCode = 500;
        if (cfg.nativeActive) {
            log.debug('[' + id + '] >>> native.handleOutgoingRequestEnd() - ERROR');
            res._JS_metadata.reqProc.handleOutgoingRequestEnd(methodId, statusCode);
        }
        log.res(statusCode, '[' + id + '] ' + outUrl.log + ' [' + statusCode + '] : ' + timer(started, 'mili').toString() + ' ms - ERROR');
    });

    waitTimeStart = timer();

    log.debug('[' + id + '] ..... http.request() <end>');
    return ret;
};


js_addListener = function(obj, args, log_functionName) {
    // args = [type, listener]
    var typeArg     = args[0];
    var listenerArg = args[1];

    log.debug('*** ' + log_functionName + ' ("' + typeArg + '", listener)');

    if (typeArg === 'request') {
        log.debug('  * Defining new request listener...');

        var new_listener = function() {
            // arguments: [req, res, next]
            var int_args = arguments;
            var req = int_args[0];
            var res = int_args[1];

            var new_listener_internal = function() {
                var id = uuid.v4();
                var nlret;
                var url_parts  = url.parse(req.url);

                session.set('id', id);
                session.set('res', res);

                // special config url
                if (url_parts.pathname === '/nodejsagentconfig') {
                    var msg = '';
                    try {
                        var tmp_query = querystring.parse(url_parts.query);
                        Object.keys(tmp_query).forEach(function(opt) {
                            msg += 'cfg.' + opt + ' : ';
                            var obj = cfg;
                            var pth = opt.split('.');
                            var lth = pth.pop();

                            Object.keys(pth).forEach(function(el) { obj = obj[pth[el]]; });

                            msg += obj[lth];
                            obj[lth] = tmp_query[opt];
                            msg += ' -> ' + obj[lth] + '\n';

                            log.info('[' + id + '] Configuration change: ' + msg);
                        });
                    }
                    catch (err) {
                        msg = 'ERROR while parsing options: ' + err;
                        log.error('[' + id + '] ' + msg);
                    }

                    res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
                    res.write(msg);
                    res.end();
                    return nlret;
                }

                // CORS signal
                if (url_parts.path === '/ruxitbeacon?dtCookie=X;XXX=X;X=X;X=X$param') {
                    var hdrOrigin = req.headers.origin || '';
                    if (hdrOrigin) {
                        var msg = '';
                        log.debug('[' + id + ']   >>> CORS signal');
                        try {
                            res.writeHead(200, "OK", {'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': hdrOrigin});
                            //res.write(msg).end();
                            res.end();
                        }
                        catch (err) {
                            msg = 'ERROR while parsing CORS request: ' + err;
                            log.error('[' + id + '] ' + msg);

                            res.writeHead(413, {'Content-Type': 'text/plain'}).end();
                            req.connection.destroy();
                        }

                        return nlret;
                    }
                }

                if (!cfg.agentActive) {
                    return args[1].apply(this, int_args);
                }

                res._JS_metadata = {
                    'id'      : id,
                    'started' : timer(),
                    'method'  : req.method,
                    'host'    : req.headers.host,
                    'url'     : url_parts.pathname,
                    'query'   : url_parts.query,
                    'headers' : req.allHeaders ? req.allHeaders.join('\r\n') : '',
                    'clientIP': getClientAddress(req),
                    'reqType' : REQUEST_TYPE.UNKNOWN,
                    'reqProc' : null,
                    'hdrArgs' : null,
                    'hdrArgsX': -1,
                    'hdrDelay': false,
                    'hdrSend' : false,
                    'hdrSent' : false,
                    'content' : {length: 0, type: '', encoding: ''},
                    'injState': INJECTION.NEED_MORE,
                    'dtCookie': parseCookie(req.headers.cookie).dtCookie || '',
                    'dtPC'    : parseCookie(req.headers.cookie).dtPC || '',
                    'sent'    : 0,
                    'buffered': 0,
                    'wasClosed': false,
                };

                log.debug('>>>>> requestHandler <begin> <<<<< [' + id + ']');
                log.debug('[' + id + '] METADATA:');
                log.debug(util.inspect(res._JS_metadata));

                log.req('[' + id + '] ' + res._JS_metadata.method + ' ' + res._JS_metadata.url + ' [---] : - ms');

                var reqType  = REQUEST_TYPE.UNKNOWN;
                var reqTypeO = {};

                if (cfg.nativeActive) {

                    var addr = {address: '0.0.0.0', port: 80, family: 'IPv4'};
                    try { addr = req.connection.server.address(); }
                    catch (err) {}

                    res._JS_metadata.reqProc = new cfg.native.RequestProcessor();
                    log.debug('[' + id + '] >>> native.handleRequest()');

                    var opt = {
                        appName         : cfg.applicationName,
                        appIP           : addr.address,
                        appPort         : addr.port,
                        scheme          : 'http',
                        method          : res._JS_metadata.method,
                        host            : res._JS_metadata.host,
                        uri             : res._JS_metadata.url,
                        query           : res._JS_metadata.query,
                        referer         : req.headers.referer,
                        headers         : req.headers,
                        headersLength   : res._JS_metadata.headers.length,
                        headersSpecial  : req.headers[cfg.specHeader],
                        dtCookie        : res._JS_metadata.dtCookie,
                        dtPC            : res._JS_metadata.dtPC,
                        dtUseDebugAgent : parseCookie(req.headers.cookie).dtUseDebugAgent || ''
                    };
                    reqTypeO = res._JS_metadata.reqProc.handleRequest(opt);

                    if (typeof reqTypeO === 'number') {
                        reqType = reqTypeO;
                    }
                    else {
                        reqType = (reqTypeO.RequestType === undefined ? REQUEST_TYPE.UNKNOWN : reqTypeO.RequestType);
                        if (!res._JS_metadata.dtCookie) {
                            res._JS_metadata.dtCookie = reqTypeO.Cookie || '';
                            if (res._JS_metadata.dtCookie) {
                                var setcookieval = res.getHeader('Set-Cookie') ? res.getHeader('Set-Cookie').split(',') : [];
                                setcookieval.push('dtCookie=' + res._JS_metadata.dtCookie + '|' + cfg.applicationName + '|1');
                                res.setHeader('Set-Cookie', setcookieval);
                            }
                        }

                        if (reqTypeO.Config && !reqTypeO.Config.UemEnabled) {
                            // UEM injection disabled
                            res._JS_metadata.injState = INJECTION.DISABLED;
                        }
                    }
                }
                else {
                    reqType = REQUEST_TYPE.REGULAR;
                }

                res._JS_metadata.reqType = reqType;

                // regular request (will be passed to application)
                if (reqType === REQUEST_TYPE.REGULAR) {

                    // x-dynatrace header - if it's present, no injection will be made
                    if (req.headers[cfg.specHeader]) { res._JS_metadata.injState = INJECTION.DISABLED; }

                    log.debug('[' + id + ']  >>> original requestHandler <begin>');
                    nlret = args[1].apply(this, int_args);
                    log.debug('[' + id + ']  >>> original requestHandler <end>');

                }

                // special request (request for JS agent)
                else if (reqType === REQUEST_TYPE.AGENT) {
                    log.info('[' + id + ']   >>> request for jsAgent : "' + res._JS_metadata.method + ' ' + res._JS_metadata.url + '"');
                    var jsAgentBody = '';
                    if (cfg.nativeActive) {
                        log.debug('[' + id + ']   >>> native.getJSAgent()');
                        jsAgentBody = res._JS_metadata.reqProc.getJSAgent();
                        if (!jsAgentBody) { log.warn('WARNING! NO jsAgent PROVIDED!'); }
                    }
                    res.writeHead(200, "OK", {'Content-Type': 'application/javascript'});
                    res.write(jsAgentBody);
                    nlret = res.end();
                }

                // special request (healthcheck page)
                else if (reqType === REQUEST_TYPE.HEALTHCHECK) {
                    // healthcheck page
                    log.debug('[' + id + ']   >>> healthcheck page: "' + res._JS_metadata.method + ' ' + res._JS_metadata.url + '"');

                    res.writeHead(200, "OK", {'Content-Type': 'text/html', 'Content-Length': Buffer.byteLength(reqTypeO.Body)});
                    res.write(reqTypeO.Body);
                    nlret = res.end();
                }

                // special request (beacon signal or healthcheck page)
                else if (reqType === REQUEST_TYPE.BEACON) {

                    var body = '';

                    // healthcheck page
                    if (querystring.parse(url_parts.query).healthcheck === '') {
                        log.debug('[' + id + ']   >>> healthcheck page: "' + res._JS_metadata.method + ' ' + res._JS_metadata.url + '"');

                        body = res._JS_metadata.reqProc.getHealthcheckPage();
                        res.writeHead(200, "OK", {'Content-Type': 'text/html', 'Content-Length': Buffer.byteLength(body)});
                        res.write(body);
                        nlret = res.end();
                    }

                    // regular beacon signal
                    else {
                        log.debug('[' + id + ']   >>> beacon signal : "' + res._JS_metadata.method + ' ' + res._JS_metadata.url + '"');

                        req.on('data', function (data) {
                          body += data;
                            // against data flooding
                            if (body.length > 1e6) {
                                body = '';
                                res.writeHead(413, {'Content-Type': 'text/plain'}).end();
                                req.connection.destroy();
                                log.error('[' + id + '] ' + res._JS_metadata.method + ' ' + res._JS_metadata.url + ' [413] : ' + timer(res._JS_metadata.started, 'mili').toString() + ' ms  - ERROR! Too big data chunk!');
                            }
                        });
                        req.on('end', function () {
                            if (cfg.nativeActive) {
                                log.debug('[' + id + ']   >>> native.beacon()');
                                var referer   = req.headers.referer || '';
                                var dtCookieX = parseCookie(req.headers.cookie).dtCookie;

                                var opt = {
                                    uri       : res._JS_metadata.host + res._JS_metadata.url,
                                    query     : res._JS_metadata.query,
                                    body      : body,
                                    referer   : referer,
                                    dtCookie  : dtCookieX,
                                    userAgent : req.headers['user-agent'],
                                    clientIP  : res._JS_metadata.clientIP
                                };
                                res._JS_metadata.reqProc.beacon(opt);

                            }
                            res.writeHead(200, 'OK', {'Content-Type': 'text/plain'});
                            res.write('OK');
                            nlret = res.end();
                        });
                    }
                }

                log.debug('[' + id + '] >>>>> requestHandler <end>');
                if (cfg.nativeActive) {
                    res._JS_metadata.reqProc.methodSuspend();
                }
                return nlret;
            };

            return session.run(new_listener_internal);
        };
        listenerArg = new_listener;
    }

    var new_args = makeArgumentsCopy.apply(this, args);
    new_args[1] = listenerArg;

    log.debug('  * Registering new listener for: ' + typeArg);
    var ret = obj.apply(this, new_args);
    log.debug('  * Done');

    return ret;
};


// hooking to module loading procedure
var _moduleLoad  = module._load;
var _instrumentedModuleLoad = function() {
    // arguments: [request, parent, isMain]
    var request = arguments[0];

    //----------------------------------------------------------------------
    var prepareParams = function(args) {
        var ret = '';
        if (args.length > 0)
            for (var i = 0; i < args.length; i++)
                ret += '     > ' + stringify(args[i]) + '\n';
        else
            ret += '     > [None]\n';
        return ret.substring(0, ret.length - 1);
    };

    var prepareHeader = function(entityName, functionName, entityObject) {
        var ret = entityName + '@' + functionName;
        if (entityObject) {
            var source = entityObject[functionName].toString().split("\n")[0];
            ret += source.substring(source.indexOf('('), source.indexOf(')') + 1);
        }
        else
            ret += '()';
        return ret;
    };

    // instrument all functions/methods in module/object
    var instrumentFunctions = function(obj, objName) {
        if (!cfg.toInstrument(objName)) {
            log.debug('     EXCLUDED from instrumentation (F)');
            return;
        }
        getNativeFunctions(obj).forEach(function(functionName) {
            var old_functionName = cfg.JS_PREFIX + functionName;
            var log_functionName = prepareHeader(objName, functionName);

            if (cfg.toInstrumentDelayed(log_functionName)) {
                log.debug('      ~~~ ' + log_functionName + ' - set to delayed instrumentation (M)');
                obj[old_functionName] = obj[functionName];

                // DELAYED INSTRUMENTATION
                obj[functionName] = function() {
                    //+++ preexecutive part
                    if (cfg.agentActive) {
                        log.debug('>>> DELAYED INSTRUMENTATION: ' + log_functionName + ' <begin>');
                        log.debug('   > PARAMS [' + arguments.length + ']:');
                        log.debug(prepareParams(arguments));
                    }
                    //--- preexecutive part

                    //+++ executive part
                    var ret;
                    if (this[old_functionName]) { ret = this[old_functionName].apply(this, arguments); }
                    //--- executive part

                    //+++ postexecutive part
                    if (cfg.agentActive) {
                        if (ret) { instrumentEntity(ret, log_functionName); }
                        log.debug('>>> DELAYED INSTRUMENTATION: ' + log_functionName + ' <end>');
                    }
                    //--- postexecutive part

                    return ret;
                };
            }
            else if (!cfg.toInstrument(log_functionName)) {
                log.debug('      !!! ' + log_functionName + ' - EXCLUDED from instrumentation (M)');
            }
            else if (typeof obj[functionName] !== 'function') {
                log.debug('      -x- ' + log_functionName + ' - is somehow not a function. Weird...');
            }
            else if (!obj[old_functionName]) {
                log.debug('      +++ ' + prepareHeader(objName, functionName, obj));
                obj[old_functionName] = obj[functionName];

                // REAL INSTRUMENTATION
                obj[functionName] = function() {

                    //+++ preexecutive part
                    if (cfg.agentActive) {
                        log.debug('>>> ' + log_functionName + ' <begin>');
                        log.debug('   > PARAMS [' + arguments.length + ']:');
                        log.debug(prepareParams(arguments));
                    }
                    //--- preexecutive part

                    //+++ executive part
                    var ret;
                    if (log_functionName in cfg.specialInstrumentedFunctions)
                        ret = global[cfg.specialInstrumentedFunctions[log_functionName]].apply(this, [this[old_functionName], arguments, log_functionName]);
                    else
                        if (this[old_functionName])
                            ret = this[old_functionName].apply(this, arguments);
                    //--- executive part

                    //+++ postexecutive part
                    if (cfg.agentActive) {
                        log.debug('>>> ' + log_functionName + ' <end>');
                    }
                    //--- postexecutive part

                    return ret;
                };
            }
            else {
                log.debug('      --- ' + log_functionName + ' - already instrumented. Aborting...');
            }
        });
    };

    // get all methods/functions/objects of the object/module
    var getEntities = function(obj, type) {
        var list = [];
        for (var f in obj) {
            try {
                if (typeof obj[f] === type)
                    list.push(f);
            }
            catch(err) {
                //log.error(err);
                continue;
            }
        }
        return list;
    };

    // get only native methods of the object (skip our wrappers)
    var getNativeFunctions = function(obj) {
        return getEntities(obj, 'function').filter( function(x) { if (!str_startsWith(x, cfg.JS_PREFIX)) return x; } );
    };

    // get only native methods/functions/objects of the object/module (skip our wrappers)
    var getNativeEntities = function(obj) {
        return getEntities(obj, 'object').filter( function(x) { if (!str_startsWith(x, cfg.JS_PREFIX)) return x; } );
    };

    // get all classes from the object/module
    var getClasses = function(entityObject) {
        var classes = [];
        for (var o in entityObject) {
            try {
                for (var x in entityObject[o].prototype) {
                    classes.push(o);
                    break;
                }
            }
            catch(err) {
                //log.error(err);
                continue;
            }
        }
        return classes;
    };

    // get only native classes from the module (skip our wrappers)
    var getNativeClasses = function(obj) {
        return getClasses(obj).filter( function(x) { if (!str_startsWith(x, cfg.JS_PREFIX)) return x; } );
    };


    var instrumentEntity = function(entityObject, entityName) {

        if ((typeof entityObject !== 'object' &&
             typeof entityObject !== 'function') ||
                entityObject === null ||
                entityObject._JS_NAME ||
                !cfg.toInstrument(entityName))
            return;

        //+++ instrumenting all internal classes +++//
        getNativeClasses(entityObject).forEach(function(internalEntityName) {
            this._JS_NAME = entityName + '@' + internalEntityName;
            if (cfg.toInstrument(this._JS_NAME)) {
                instrumentEntity(entityObject[internalEntityName].prototype, this._JS_NAME);
            }
            else {
                log.debug('   [' + this._JS_NAME + '] - EXCLUDED from instrumentation (C)');
            }
            log.debug('');
        }, entityObject);
        //--- instrumenting all internal classes ---//

        //+++ instrumenting all module objects +++//
        getNativeEntities(entityObject, 'object').forEach(function(internalObjectName) {
            this._JS_NAME = entityName + '@' + internalObjectName;
            if (cfg.toInstrument(this._JS_NAME))
                instrumentEntity(entityObject[internalObjectName], this._JS_NAME);
            else
                log.debug('   [' + this._JS_NAME + '] - EXCLUDED from instrumentation (O)');
            log.debug('');
        }, entityObject);
        //--- instrumenting all module objects ---//

        //+++ instrumenting all object methods +++//
        log.debug('   [' + entityName + '] METHODS instrumentation');
        instrumentFunctions(entityObject, entityName);
        log.debug('');
        //--- instrumenting all object methods ---//
    };
    //----------------------------------------------------------------------


    var m = _moduleLoad.apply(this, arguments);

    if (!m) {
        log.warn('WARNING! Module [' + request + '] has not been found. Skipping...');
    }
    if (typeof m === 'function' && cfg.toInstrumentDelayed(request)) {
        return function() {
            //+++ preexecutive part
            if (cfg.agentActive) {
                log.debug('<--begin--> [' + request + '] MODULE delayed instrumentation');
                log.debug('   > PARAMS [' + arguments.length + ']:');
                log.debug(prepareParams(arguments));
            }
            //--- preexecutive part

            //+++ executive part
            var ret = m.apply(this, arguments);
            //--- executive part

            //+++ postexecutive part
            if (cfg.agentActive) {
                if (ret) { instrumentEntity(ret, request); }
                log.debug('<---end---> [' + request + '] MODULE delayed instrumentation');
            }
            //--- postexecutive part

            return ret;
        };
    }
    else if (cfg.toInstrument(request)) {
        log.debug('<--begin--> [' + request + '] MODULE instrumentation');
        instrumentEntity(m, request);
        log.debug('<---end---> [' + request + '] MODULE instrumentation');
    }
    else
        log.debug('<-excluded-> [' + request + '] MODULE instrumentation');
    return m;
};


//====================================================================================================================
// START
//====================================================================================================================
var cfg = new JSAconfig();
var log = cfg.logger;

log.info('=== Agent [start] ===');

log.info('  * Platform : ' + global.process.platform);
log.info('  * Arch     : ' + global.process.arch);
log.info('  * Node ver.: ' + global.process.version);

if (cfg.nativeActive) {
    var lib_path = '../lib';
    if (global.process.arch === 'x64') { lib_path += '64'; }

    try {
        log.info('*** Loading native extension ***');
        cfg.native   = require(lib_path + '/' + cfg.nativelib);
        cfg.cfgAgent = new cfg.native.CommHandler(options);
    }
    catch (err) {
        log.error('ERROR! Agent injection aborted: ' + err.toString());
        return;
    }
}

process.on('SIGINT', function() {
    log.warn("SHUTTING DOWN THE NODEJS AGENT");

    if (cfg.nativeActive) {
        cfg.cfgAgent.shutdown();
    }

    // if there's only one (this one) callback for SIGING defined, then we have to exit;
    // otherwise - we depend on the listeners defined in monitored application
    if (process.EventEmitter.listenerCount(process, 'SIGINT') === 1) {
        process.exit();
    }
});

log.info('*** Hooking to module load procedure ***');
module._load = _instrumentedModuleLoad;

log.info('--- Agent ver.: ' + verID + ' ---');
log.info('Agent options: ' + util.inspect(options));
log.info('=== Agent [ end ] ===');
log.info('');
log.info('=== Application [ ' + cfg.applicationName + ' ] start ===');

};

//====================================================================================================================
// END
//====================================================================================================================
// (1) do not reimplement add_listener; reimplement createServer instead (really? need to check this)
